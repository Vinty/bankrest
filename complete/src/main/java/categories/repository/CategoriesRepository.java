package categories.repository;

import categories.entity.Categories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface CategoriesRepository extends CrudRepository<Categories, Long> {

}
