package categories.repository;

import categories.entity.Categories;
import categories.entity.TemplatesType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface TemplatesTypeRepository extends CrudRepository<TemplatesType, Long> {

}
