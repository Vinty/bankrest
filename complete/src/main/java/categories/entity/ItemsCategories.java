package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class ItemsCategories {
  private Long id;
  private Type type;
  private String reportName;
  private LocalDateTime dateTime;
  private String operator;
}
