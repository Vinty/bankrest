package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Template {
  private Long id;
  private Type type;
  private String description;
  private LocalDateTime createdAt;
  private LocalDateTime modifiedAt;
  private LocalDateTime activatedAt;
}
