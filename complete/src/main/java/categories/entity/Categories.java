package categories.entity;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "categories")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Categories extends BaseEntity {
    @Column(name = "name")
    private String name;
    @ManyToOne
    @JoinColumn(name = "type_id")
    private Type typeId;

    public Categories(Long id, String name, Type typeId) {
        super(id);
        this.name = name;
        this.typeId = typeId;
    }
}
