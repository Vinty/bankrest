package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class TypePrimitive {
  private Long id;
  private String name;
}
