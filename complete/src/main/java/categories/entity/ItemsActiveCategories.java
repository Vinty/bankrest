package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class ItemsActiveCategories {
  private Long id;
  private Type type;
  private String name;
}
