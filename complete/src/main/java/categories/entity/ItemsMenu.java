package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ItemsMenu {
    private Long id;
    private String item;
    private String value;
    private TypePrimitive primitiveType;

  public ItemsMenu(Long id, String item, String value) {
    this.id = id;
    this.item = item;
    this.value = value;
  }
}
