package categories.entity;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "templates_type")
@ToString
public class TemplatesType extends BaseEntity {
    @Column(name = "name")
    private String name;

    public TemplatesType(Long id, String name) {
        super(id);
        this.name = name;
    }
}
