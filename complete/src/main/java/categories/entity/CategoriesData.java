package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class CategoriesData {
  private Long id;
  private List<ItemsCategories> itemsCategories;
}
