package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Menus {
  private Long id;
  private String name;
  private List<ItemsMenu> itemsMenu;
}
