package categories.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class ActiveCategories {
    private Long id;
    private List<ItemsActiveCategories> itemsActiveCategories;

  public ActiveCategories(List<ItemsActiveCategories> itemsActiveCategories) {
    this.itemsActiveCategories = itemsActiveCategories;
  }
}
