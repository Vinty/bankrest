package categories.controllers;

import categories.entity.Categories;
import categories.entity.Type;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Controller
public class CategoriesController {

  private static final String template = "Hello, %s!";
  private final AtomicLong counter = new AtomicLong();

  @RequestMapping(path = "/")
  public String index() {
    return "index";
  }

  @GetMapping("/categories")
  @ResponseBody
  public List<Categories> getJSONCategories() {
    List<Categories> list = new ArrayList<>();
    list.add(new Categories(1L, "Переводы юр. лиц", new Type("entity_transfer")));
    list.add(new Categories(2L, "Перевод на депозит в другой банк", new Type("deposits_transfer")));
    list.add(new Categories(3L, "Среднедневные остатки за n-месяц", new Type("avarage_day_remnants")));
    list.add(new Categories(4L, "Контрагенты по поступлениям", new Type("agents_geton")));
    list.add(new Categories(5L, "Контрагенты по списаниям", new Type("agents_takeoff")));
    list.add(new Categories(6L, "Заработной платы проект", new Type("payment_project")));
    list.add(new Categories(7L, "Затухание или развитие клиента", new Type("client_evolve")));
    return list;
  }

//    @RequestMapping("/greeting")
//    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
//        return new Greeting(counter.incrementAndGet(), String.format(template, name));
//    }
}
