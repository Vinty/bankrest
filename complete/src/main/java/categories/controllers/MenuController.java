package categories.controllers;

import categories.entity.ItemsMenu;
import categories.entity.Menus;
import categories.entity.TypePrimitive;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MenuController {

  @GetMapping("/menus")
  @ResponseBody
  public List<Menus> getJSONListActiveCategories() {
    List<Menus> menusList = new ArrayList<>();

    List<ItemsMenu> itemsMenus = new ArrayList<>();
    itemsMenus.add(new ItemsMenu(1L, "id", "№ п.п."));
    itemsMenus.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента"));
    itemsMenus.add(new ItemsMenu(3L, "unp", "УНП"));
    itemsMenus.add(new ItemsMenu(4L, "transfer_sum", "Сумма переводов"));
    itemsMenus.add(new ItemsMenu(5L, "clients_count", "Количество связанных клиентов банка"));
    itemsMenus.add(new ItemsMenu(6L, "goal", "Задача"));

    List<ItemsMenu> itemsMenus1 = new ArrayList<>();
    itemsMenus1.add(new ItemsMenu(1L, "id", "№ п.п."));
    itemsMenus1.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента"));
    itemsMenus1.add(new ItemsMenu(3L, "unp", "УНП"));
    itemsMenus1.add(new ItemsMenu(4L, "transfer_sum", "Сумма переводов"));
    itemsMenus1.add(new ItemsMenu(5L, "clients_count", "Количество связанных клиентов банка"));
    itemsMenus1.add(new ItemsMenu(6L, "goal", "Задача"));

    List<ItemsMenu> itemsMenus7 = new ArrayList<>();
    itemsMenus7.add(new ItemsMenu(1L, "id", "№ п.п."));
    itemsMenus7.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента"));
    itemsMenus7.add(new ItemsMenu(3L, "unp", "УНП"));
    itemsMenus7.add(new ItemsMenu(4L, "transfer_sum", "Сумма переводов"));
    itemsMenus7.add(new ItemsMenu(5L, "clients_count", "Количество связанных клиентов банка"));
    itemsMenus7.add(new ItemsMenu(6L, "goal", "Задача"));

    List<ItemsMenu> itemsMenus3 = new ArrayList<>();
    itemsMenus3.add(new ItemsMenu(1L, "id", "№ п.п."));
    itemsMenus3.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента"));
    itemsMenus3.add(new ItemsMenu(3L, "unp", "УНП"));
    itemsMenus3.add(new ItemsMenu(4L, "currency", "Валюта"));
    itemsMenus3.add(new ItemsMenu(5L, "all_payment", "Всего на з/п"));
    itemsMenus3.add(new ItemsMenu(6L, "transfer_sum", "Безналичное перечисление"));
    itemsMenus3.add(new ItemsMenu(7L, "cash_getting", "Снятие наличными"));
    itemsMenus3.add(new ItemsMenu(8L, "fot_sum", "Примерный размер ФОТ (из суммы ФСЗН)"));
    itemsMenus3.add(new ItemsMenu(9L, "goal", "Задача"));

    List<ItemsMenu> itemsMenus4 = new ArrayList<>();
    itemsMenus4.add(new ItemsMenu(1L, "id", "№ п.п."));
    itemsMenus4.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента"));
    itemsMenus4.add(new ItemsMenu(3L, "unp", "УНП"));
    itemsMenus4.add(new ItemsMenu(4L, "currency", "Валюта"));
    itemsMenus4.add(new ItemsMenu(5L, "all_payment", "Всего на з/п"));
    itemsMenus4.add(new ItemsMenu(6L, "transfer_sum", "Безналичное перечисление"));
    itemsMenus4.add(new ItemsMenu(7L, "cash_getting", "Снятие наличными"));
    itemsMenus4.add(new ItemsMenu(8L, "fot_sum", "Примерный размер ФОТ (из суммы ФСЗН)"));
    itemsMenus4.add(new ItemsMenu(9L, "goal", "Задача"));

    List<ItemsMenu> itemsMenus5 = new ArrayList<>();
    itemsMenus5.add(new ItemsMenu(1L, "id", "№ п.п."));
    itemsMenus5.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента"));
    itemsMenus5.add(new ItemsMenu(3L, "unp", "УНП"));
    itemsMenus5.add(new ItemsMenu(4L, "avarage_day_remnants", "Среднедневной остаток"));
    itemsMenus5.add(new ItemsMenu(5L, "currency", "Валюта"));
    itemsMenus5.add(new ItemsMenu(6L, "transfer_sum", "Безналичное перечисление"));
    itemsMenus5.add(new ItemsMenu(7L, "goal", "Задача"));

    List<ItemsMenu> itemsMenus2 = new ArrayList<>();
    itemsMenus2.add(new ItemsMenu(1L, "id", "№ п.п.", new TypePrimitive(1L, "number")));
    itemsMenus2.add(new ItemsMenu(2L, "agent_name", "Наименование контрагента", new TypePrimitive(2L, "String")));
    itemsMenus2.add(new ItemsMenu(3L, "unp", "УНП", new TypePrimitive(1L, "number")));
    itemsMenus2.add(new ItemsMenu(4L, "transfer_sum", "Сумма переводов", new TypePrimitive(1L, "number")));
    itemsMenus2.add(new ItemsMenu(5L, "transaction_count", "Количество транзакций", new TypePrimitive(1L, "number")));
    itemsMenus2.add(new ItemsMenu(6L, "goal", "Задача", new TypePrimitive(2L, "String")));

    menusList.add(new Menus(1L, "agents_takeoff", itemsMenus));
    menusList.add(new Menus(2L, "agents_geton", itemsMenus1));
    menusList.add(new Menus(3L, "payment_project", itemsMenus3));
    menusList.add(new Menus(4L, "client_evolve", itemsMenus4));
    menusList.add(new Menus(5L, "avarage_day_remnants", itemsMenus5));
    menusList.add(new Menus(6L, "deposits_transfer", itemsMenus7));
    menusList.add(new Menus(7L, "entity_transfer", itemsMenus2));
    return menusList;
  }
}
