package categories.controllers;

import categories.entity.TemplatesType;
import categories.services.TemplatesTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TemplatesTypeController {

    @Autowired
    private TemplatesTypeService templatesTypeService;

    @RequestMapping(value = "/templates_type/", method = RequestMethod.GET)
    public List<TemplatesType> getJSONTemplatesType() {
        return templatesTypeService.findAll();
    }
}
