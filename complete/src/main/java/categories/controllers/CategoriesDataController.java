package categories.controllers;

import categories.entity.CategoriesData;
import categories.entity.ItemsCategories;
import categories.entity.Type;
import categories.helpers.DateTimeHelper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class CategoriesDataController {

  @GetMapping("/categoriesData")
  @ResponseBody
  public List<CategoriesData> getJSONListCategoriesData() {
    LocalDateTime localDateTime = DateTimeHelper.getLocalDateTime();

    List<CategoriesData> list = new ArrayList<>();

    list.add(new CategoriesData(1L, Arrays.asList(new ItemsCategories(1L, new Type("entity_transfer"), "Перевод в рамках одного ЮЛ",localDateTime , "Администратор"))));
    list.add(new CategoriesData(2L, Arrays.asList(new ItemsCategories(2L, new Type("deposits_transfer"), "Переводы на депозит в другой банк", localDateTime, "Администратор"))));
    list.add(new CategoriesData(3L, Arrays.asList(new ItemsCategories(3L, new Type("avarage_day_remnants"), "Среднедневные остатки за n-месяц", localDateTime, "Администратор"))));
    list.add(new CategoriesData(4L, Arrays.asList(new ItemsCategories(4L, new Type("agents_geton"), "Среднедневные остатки за n-месяц", localDateTime, "Администратор"))));
    list.add(new CategoriesData(5L, Arrays.asList(new ItemsCategories(5L, new Type("agents_takeoff"), "Среднедневные остатки за n-месяц", localDateTime, "Администратор"))));
    list.add(new CategoriesData(6L, Arrays.asList(new ItemsCategories(6L, new Type("payment_project"), "Среднедневные остатки за n-месяц", localDateTime, "Администратор"))));
    list.add(new CategoriesData(7L, Arrays.asList(new ItemsCategories(7L, new Type("client_evolve"), "Среднедневные остатки за n-месяц", localDateTime, "Администратор"))));
    return list;
  }

//    @RequestMapping("/greeting")
//    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
//        return new Greeting(counter.incrementAndGet(), String.format(template, name));
//    }
//  }
}
