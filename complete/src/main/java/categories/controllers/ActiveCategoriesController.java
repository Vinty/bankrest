package categories.controllers;

import categories.entity.ActiveCategories;
import categories.entity.ItemsActiveCategories;
import categories.entity.Type;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class ActiveCategoriesController {

  @GetMapping("/activeCategories")
  @ResponseBody
  public List<ActiveCategories> getJSONListActiveCategories() {
    List<ActiveCategories> list = new ArrayList<>();

    List<ItemsActiveCategories> itemsActiveCategories = new ArrayList<>();
    itemsActiveCategories.add(new ItemsActiveCategories(1L, new Type("avarage_day_remnants"), "Среднедневные остатки за n-месяц"));
    itemsActiveCategories.add(new ItemsActiveCategories(2L, new Type("agents_geton"), "Контрагенты по поступлениям"));
    itemsActiveCategories.add(new ItemsActiveCategories(3L, new Type("agents_takeoff"), "Контрагенты по списаниям"));
    itemsActiveCategories.add(new ItemsActiveCategories(4L, new Type("payment_project"), "Заработной платы проект"));
    itemsActiveCategories.add(new ItemsActiveCategories(5L, new Type("client_evolve"), "Затухание или развитие клиента"));
    list.add(new ActiveCategories(1L, itemsActiveCategories));
    return list;
  }
}
