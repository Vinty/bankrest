package categories.services;

import categories.entity.TemplatesType;
import categories.repository.TemplatesTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class TemplatesTypeServiceImpl implements TemplatesTypeService{

    @Autowired
    private TemplatesTypeRepository templatesTypeRepository;

    @Override
    public List<TemplatesType> findAll() {
        return (List<TemplatesType>) templatesTypeRepository.findAll();
    }
}
