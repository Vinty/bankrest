package categories.services;

import categories.entity.TemplatesType;

import java.util.List;

public interface TemplatesTypeService {

    List<TemplatesType> findAll();

}
