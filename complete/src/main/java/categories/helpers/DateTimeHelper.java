package categories.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeHelper {

  public static void main(String[] args) {
    String dateTimeFromDB = "2016-11-09 10:30:21";
    System.out.println(getLocalDate(getDateTime()));
    System.out.println(getLocalTime(getDateTime()));
    System.out.println(getLocalDateTime());
    System.out.println(getLocalDateTimeStr(getLocalDateTime()));

    System.out.println("Из БД - получаем дату " + getLocalDateTimeFromDB(dateTimeFromDB));
  }

  public static Instant getDateTime() {
    return Instant.now();
  }

  public static LocalDate getLocalDate(Instant instant) {
    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate();
  }

  public static LocalTime getLocalTime(Instant instant) {
    return LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime();
  }

  public static LocalDateTime getLocalDateTime() {
    return LocalDateTime.now();
  }

  public static String getLocalDateTimeStr(LocalDateTime localDateTime) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    return localDateTime.format(formatter);
  }

  public static LocalDateTime getLocalDateTimeFromDB(String dateTimeFromDB) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    return LocalDateTime.parse(dateTimeFromDB, formatter);
  }
}
