package categories.repository;

import categories.entity.Categories;
import config.RepositoryConfigurationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

public class CategoriesRepositoryTest extends CommonTest{

    @Autowired
    private CategoriesRepository categoriesRepository;

    @Test
    public void findAllTest() {
        Iterable<Categories> all = categoriesRepository.findAll();
        System.out.println(all);
    }
}