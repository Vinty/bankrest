package categories.repository;

import categories.entity.TemplatesType;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TemplatesTypeRepositoryTest extends CommonTest{

    @Autowired
    private TemplatesTypeRepository templatesTypeRepository;

    @Test
    public void findAllTest() {
        Iterable<TemplatesType> all = templatesTypeRepository.findAll();
        Assert.assertNotNull(all);
    }
}